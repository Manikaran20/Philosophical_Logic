theory Meanings
imports Main
begin
typedecl \<mu> --"type for meanings"
consts literal :: "\<mu>\<Rightarrow>bool"
consts intended :: "\<mu> \<Rightarrow>bool"
axiomatization where  asserted : "asserted x \<Longrightarrow> intended x"
axiomatization where expressed : "expressed x \<Longrightarrow> literal x"
end


